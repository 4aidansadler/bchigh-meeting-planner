use reqwest::header::AUTHORIZATION;
use read_input::prelude::*;
#[path = "structs.rs"]
pub mod structs;

#[tokio::main]
async fn authorize_api(code: String) -> Result<String, reqwest::Error>{
    let client = reqwest::Client::new();
    let res = client
        .post("https://zoom.us/oauth/token")
        .header(AUTHORIZATION, "Basic YUJ1eVE1alF2R2VsMmlpRXJ5RzZnOlJIdnVGRzZWT2NpRnhqVVJKN0doQm5FMkVIMFdHM0Fk")
        .query(&[("grant_type", "authorization_code"), ("code", &code), ("redirect_uri", "https://pleasecopythecode.com")])
        .send()
        .await
        .unwrap()
        .text()
        .await?;
    Ok(res)
}

pub fn get_auth() -> structs::AuthTokens {
    let res = authorize_api(input().msg("Please input your code: ").get());
    let res_final: structs::AuthTokens = serde_json::from_str(&res.unwrap()).unwrap();
    return res_final;
}

#[tokio::main]
pub async fn get_usr_info(token: &String) -> Result<structs::UserInfo, reqwest::Error> {
    let client = reqwest::Client::new();
    let res = client.get("https://api.zoom.us/v2/users/me")
        .header(AUTHORIZATION, format!("Bearer {}", token))
        .send()
        .await
        .unwrap()
        .text()
        .await?;
    let res_final: structs::UserInfo = serde_json::from_str(&res).unwrap();
    Ok(res_final)
}

#[tokio::main]
pub async fn refresh_auth(old_tokens: structs::AuthTokens) -> Result<structs::AuthTokens, reqwest::Error> {
    let client = reqwest::Client::new();
    let res = client.post("https://zoom.us/oauth/token")
        .header(AUTHORIZATION, "Basic YUJ1eVE1alF2R2VsMmlpRXJ5RzZnOlJIdnVGRzZWT2NpRnhqVVJKN0doQm5FMkVIMFdHM0Fk")
        .query(&[("grant_type", "refresh_token"), ("refresh_token", &old_tokens.refresh_token)])
        .send()
        .await
        .unwrap()
        .text()
        .await?;
    let new_tokens: structs::AuthTokens = serde_json::from_str(&res).unwrap();
    Ok(new_tokens)
}
