use std::fs::*;
use std::fs;
use read_input::prelude::*;
use std::path::Path;
use std::io::prelude::*;
mod access_api;

static LINK: &str = "https://zoom.us/oauth/authorize?response_type=code&client_id=aBuyQ5jQvGel2iiEryG6g&redirect_uri=https://pleasecopythecode.com";
fn main() {
    println!("--------------------------");
    println!("bchigh-meeting-planner\nAidan Sadler 2021\nProof-Of-Concept");
    println!("--------------------------");
    let tokens: access_api::structs::AuthTokens;
    if Path::new("tokens.json").exists() == true {
        println!("Looks like you have used this tool befoe,");
        let q: String = input().msg("Would you like to use your previous login?(y/any other key)").get();
        if q == "y" || q == "Y"{
            let token_str = fs::read_to_string("tokens.json");
            let old_tokens: access_api::structs::AuthTokens = serde_json::from_str(&token_str.unwrap()).unwrap();
            tokens = access_api::refresh_auth(old_tokens).unwrap();
        }else {
            usr_instructions();
            tokens = access_api::get_auth();
        }
    }else{
        println!("Looks like you have not ran this tool before,");
        usr_instructions();
        tokens = access_api::get_auth();
    }
    write_data(serde_json::to_string(&tokens).unwrap(), "tokens.json").unwrap();
    let usr_info = access_api::get_usr_info(&tokens.access_token).unwrap();
    println!("Welcome {} {}!", usr_info.first_name, usr_info.last_name);
    println!("(Hint: you can use the \"help\" command to get a list of possible commands)");
    usr_input(&tokens);
}

fn usr_instructions(){
    println!("To start please use your browser to go to:\n{}", LINK);
    println!("After you complete the authorization it will reirect you to a random link,");
    println!("In that url it will say somewhere '&code=',");
}

fn write_data(data: String, file_name: &str) -> std::io::Result<String> {
    let mut file = File::create(file_name)?;
    file.write_all(format!("{}", data).as_bytes())?;
    Ok(String::from("Data save success!"))
}

fn usr_input(t: &access_api::structs::AuthTokens) {
    let i: String = input().msg("what do you want to do: ").get();
    if i == String::from("help"){
        println!(" plan - plan multible meetings with certain parameters(Not Implemented yet)\n usr_info - get all your zoom account info\n exit - exit this tool\n help - get a list of all commands");
    }else if i == String::from("plan"){
        println!("Error: Command not implemented yet")
    }else if i == String::from("usr_info") {
        println!("Here is your account info:\n{}", serde_json::to_string(&access_api::get_usr_info(&t.access_token).unwrap()).unwrap());
    }else if i == String::from("exit"){
        std::process::exit(0x0100);
    }
    usr_input(t);
}