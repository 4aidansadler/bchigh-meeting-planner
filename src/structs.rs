use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone)]
pub struct AuthTokens {
    pub access_token: String,
    pub token_type: String,
    pub refresh_token: String,
    pub expires_in: i32,
    pub scope: String
}

#[derive(Serialize, Deserialize, Clone)]
pub struct UserInfo {
    pub id: String,
    pub first_name : String,
    pub last_name: String,
    pub email: String,
    pub r#type: i8,
    pub role_name: String,
    pub pmi: i64,
    pub use_pmi: bool,
    pub personal_meeting_url: String,
    pub timezone: String,
    pub verified: i8,
    pub dept: String,
    pub created_at: String,
    pub last_login_time: String,
    pub last_client_version: String,
    pub pic_url: String,
    pub host_key: String,
    pub jid: String,
    pub group_ids: Vec<String>,
    pub im_group_ids: Vec<String>,
    pub account_id: String,
    pub language: String,
    pub phone_country: String,
    pub phone_number: String,
    pub status: String
}